<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arr = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'uses' => $this->uses,
            'side_effects' => $this->side_effects,
            'min_quantity' => $this->min_quantity,
            'status' => $this->status == 1,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
        if (!is_null($this->dispensed_count)) {
            $arr['actionable'] = $this->dispensed_count < 1;
        }

        return $arr;
    }
}
