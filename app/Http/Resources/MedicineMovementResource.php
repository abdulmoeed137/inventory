<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MedicineMovementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arr = [
            'medicine' => $this->medicine,
            'quantity' => $this->quantity,
            'department' => $this->department,
            'dispensed_at' => $this->dispensed_at
        ];
        $d = Carbon::parse($this->dispensed_at);
        if ($d->gte(now()->startOfWeek()) && $d->lte(now()->endOfWeek())) {
            $arr['movement'] = 'Weekly';
        } else if ($d->format('Y') != now()->format('Y')) {
            $arr['movement'] = 'Annualy';
        } else if ($d->format('m-Y') == now()->format('m-Y')) {
            $arr['movement'] = 'Daily';
        } else  if ($d->format('m') != now()->format('m') && $d->format('Y') == now()->format('Y')) {
            $arr['movement'] = 'Monthly';
        }
        return $arr;
    }
}
