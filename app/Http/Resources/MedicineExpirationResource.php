<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MedicineExpirationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'medicine' => $this->medicine->name,
            'patch_no' => $this->patch_no,
            'quantity' => $this->quantity,
            'status' => Carbon::parse($this->expired_at)->lte(now()) ? 'Expired' : 'Not Expired',
            'expired_at' => Carbon::parse($this->expired_at)->format('d-m-Y'),
        ];
    }
}
