<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicineRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arr = [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'type' => $this->type == 1,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'medicine' => new SimpleResource($this->whenLoaded('medicine')),
            'department' => new SimpleResource($this->whenLoaded('department')),
            'approver' => new SimpleResource($this->whenLoaded('approver')),
            'requester' => new SimpleResource($this->whenLoaded('requester')),
            'hod' => new SimpleResource($this->whenLoaded('hod')),
            'hon' => new SimpleResource($this->whenLoaded('hon')),
            'pharmacist' => new SimpleResource($this->whenLoaded('pharmacist'))
        ];

        if (!is_null($this->given_quantity)) {
            $arr['is_completed'] = $this->given_quantity == $this->quantity;
            $arr['given'] = $this->given_quantity;
            $arr['status'] = !$this->approver_id ? 'Unapproved' : ($arr['given']  < 1 ? 'Approved' : ($arr['is_completed'] ? 'Dispensed' : 'Partially Dispensed'
                ));
        }
        return $arr;
    }
}
