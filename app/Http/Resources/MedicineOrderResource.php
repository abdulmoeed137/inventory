<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicineOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arr = [
            'id' => $this->id,
            'total_price' => $this->total_price,
            'quantity' => $this->quantity,
            'supplied_at' => $this->supplied_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'medicine' => $this->whenLoaded('medicine', fn () => collect(new MedicineResource($this->medicine))->filter(fn ($c) => !!$c)),
            'supplier' => $this->whenLoaded('supplier', fn () => collect(new SupplierResource($this->supplier))->filter(fn ($c) => !!$c)),
        ];

        if (!is_null($this->inventory_count) && !is_null($this->dispensed_count)) {
            $arr['status'] = $this->inventory_count > 0;
            $arr['actionable'] = $this->dispensed_count < 1;
        }

        return $arr;
    }
}
