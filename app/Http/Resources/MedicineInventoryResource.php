<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicineInventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'patch_no' => $this->patch_no,
            'quantity' => $this->quantity,
            'expired_at' => $this->expired_at,
            'produced_at' => $this->produced_at,
            'actionable' => !$this->dispensings()->exists(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'medicine' => $this->whenLoaded('medicine', fn () => collect(new MedicineResource($this->medicine))->filter(fn ($c) => !!$c)),
        ];
    }
}
