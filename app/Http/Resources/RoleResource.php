<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'isUser' => $this->is_user == 1,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'panels' => PanelResource::collection($this->whenLoaded('panels')),
            'departments' => DepartmentResource::collection($this->whenLoaded('departments')),
            'permissions' => $this->whenLoaded('permissions', fn () => $this->permissions->pluck('id')->toArray()),
        ];
    }
}
