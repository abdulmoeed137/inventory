<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'locale' => $this->locale,
            'status' => !!$this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'items' => ItemResource::collection($this->whenLoaded('items')),
            'suppliers' => SupplierResource::collection($this->whenLoaded('suppliers')),
        ];
    }
}
