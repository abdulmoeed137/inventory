<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EquipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'serial_no' => $this->serial_no,
            'model' => $this->model,
            'warrenty' => $this->warrenty,
            'installation_date' => $this->installation_date,
            'status' => $this->status == 1,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'serviceCost' => $this->when(!is_null($this->serviceCost), $this->serviceCost),
            'sub_category' => new SubCategoryResource($this->whenLoaded('subCategory')),
            'department' => new DepartmentResource($this->whenLoaded('department')),
            'supplier_engineer' => new SupplierEngineerResource($this->whenLoaded('supplierEngineer')),
            'latest_service' => new EquipmentServiceResource($this->whenLoaded('latestService')),
            'services' => EquipmentServiceResource::collection($this->whenLoaded('services'))
        ];
    }
}
