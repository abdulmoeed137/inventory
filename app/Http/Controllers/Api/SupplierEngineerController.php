<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierEngineerResource;
use App\Models\Supplier;
use App\Models\SupplierEngineer;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SupplierEngineerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Supplier $supplier)
    {
        return response([
            'engineers' => SupplierEngineerResource::collection($supplier->engineers()->latest()->get())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:supplier_engineers,email',
                'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:supplier_engineers,phone',
                'supplier' => 'required|integer|exists:suppliers,id',
            ]);

            $validated['supplier_id'] = $validated['supplier'];
            unset($validated['supplier']);
            $engineer = SupplierEngineer::create($validated);
            return response()->json([
                'message' => 'Supplier engineer created successfully.',
                'success' => true,
                'engineer' => new SupplierEngineerResource($engineer)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupplierEngineer  $supplierEngineer
     * @return \Illuminate\Http\Response
     */
    public function show(SupplierEngineer $supplierEngineer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SupplierEngineer  $supplierEngineer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $engineer = SupplierEngineer::findOrFail($id);
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:supplier_engineers,email,' . $id,
                'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:supplier_engineers,phone,' . $id,
            ]);
            $engineer->update($validated);
            return response()->json([
                'message' => 'Supplier engineer updated successfully.',
                'success' => true,
                'engineer' => new SupplierEngineerResource($engineer)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupplierEngineer  $supplierEngineer
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupplierEngineer $supplierEngineer)
    {
        //
    }
}
