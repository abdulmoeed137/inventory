<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\MedicineRequestResource;
use App\Http\Resources\MedicineResource;
use App\Http\Resources\SimpleResource;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Medicine;
use App\Models\MedicineRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class MedicineRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::latest()->get();
        $medicines = Medicine::latest()->get();
        $requests = MedicineRequest::withAllRelations();
        $pharmacists = Employee::select('id', 'name', 'role_id')->where('role_id', 6)->get();

        return [
            'departments' => DepartmentResource::collection($departments),
            'medicines' => MedicineResource::collection($medicines),
            'requests' => MedicineRequestResource::collection($requests),
            'pharmacists' => SimpleResource::collection($pharmacists)
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'department' => 'required|integer|exists:departments,id',
                'requester' => 'required|integer|exists:employees,id',
                'hod' => 'required|integer|exists:employees,id',
                'hon' => 'required|integer|exists:employees,id',
                'pharmacist' => 'required|integer|exists:employees,id',
                'medicine' => 'required|integer|exists:medicines,id',
                'quantity' => 'required|integer|min:1',
                'type' => 'nullable'
            ], [], [
                'hod' => 'head_of_department',
                'hon' => 'head_of_nurses',
            ]);

            foreach (array_diff_key($validated, array_flip(['type', 'quantity'])) as $key => $validate) {
                $validated["{$key}_id"] = $validate;
                unset($validated[$key]);
            }
            $validated['type'] = !!$validated['type'];
            $req = MedicineRequest::create($validated);
            return response()->json([
                'message' => 'Medicine request created successfully.',
                'success' => true,
                'request' => new MedicineRequestResource($req->loadAllRelations())
            ], 201);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MedicineRequest  $medicineRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $req = MedicineRequest::findOrFail($id);
            $validated = $request->validate([
                'department' => 'required|integer|exists:departments,id',
                'requester' => 'required|integer|exists:employees,id',
                'hod' => 'required|integer|exists:employees,id',
                'hon' => 'required|integer|exists:employees,id',
                'pharmacist' => 'required|integer|exists:employees,id',
                'medicine' => 'required|integer|exists:medicines,id',
                'quantity' => 'required|integer|min:1',
                'type' => 'nullable'
            ], [], [
                'hod' => 'head_of_department',
                'hon' => 'head_of_nurses',
            ]);

            foreach (array_diff_key($validated, array_flip(['type', 'quantity'])) as $key => $validate) {
                $validated["{$key}_id"] = $validate;
                unset($validated[$key]);
            }
            $validated['type'] = !!$validated['type'];
            $req->update($validated);
            return response()->json([
                'message' => 'Medicine request updated successfully.',
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicineRequest  $medicineRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicineRequest $medicineRequest)
    {
        //
    }

    public function approve($id)
    {
        $req = MedicineRequest::findOrFail($id);
        $approver = auth()->user()->employee;
        $req->update(['approver_id' => $approver->id]);

        return response()->json([
            'message' => 'Medicine request approved successfully.',
            'approver' => new SimpleResource($approver),
            'success' => true
        ], 200);
    }

    public function dispencing(Request $request, $id)
    {
        $resp = ['success' => true];
        $req = MedicineRequest::withSum(['dispensings as quantity' => fn ($q) => $q->select(DB::raw('medicine_requests.quantity-CAST(IFNULL(SUM(medicine_dispensings.quantity), 0) AS UNSIGNED)'))], 'medicine_dispensings.quantity')
            ->findOrFail($id);
        $need = $req->quantity - $req->given;
        $medicine = Medicine::with(['inventories' => function ($q) {
            $q->where(function ($q) {
                $q->whereHas('dispensings', function ($q) {
                    $q->groupBy('medicine_dispensings.request_id')
                        ->havingRaw('sum(medicine_dispensings.quantity) < medicine_inventories.quantity');
                })->orDoesntHave('dispensings');
            })->select('id', 'medicine_id', 'created_at')
                ->withSum(['dispensings as quantity' => fn ($q) => $q->select(DB::raw('medicine_inventories.quantity-CAST(IFNULL(SUM(medicine_dispensings.quantity), 0) AS UNSIGNED)'))], 'medicine_dispensings.quantity')
                ->oldest();
        }])->find($req->medicine_id);

        if (($medicine->inventories->sum('quantity') <= $medicine->min_quantity) && !$request->has('partial')) {
            // notification here
            $resp['text'] = "The Medicine quantity requested is large which will take some time to receive. Kindly make an Order of $req->quantity to dispense the request.";
            $resp['success'] = false;
        } else if ((($medicine->inventories->sum('quantity') - $req->quantity) <= $medicine->min_quantity) && !$request->has('partial')) {
            // notification here
            $resp['text'] = 'The Medicine quantity requested can be partially fulfilled right now. You have to make an Order for ' .
                ($req->quantity - ($medicine->inventories->sum('quantity') - $medicine->min_quantity)) .
                ' more to complete the quantity and dispense the request.';
        } else {
            $rem = $req->quantity;
            if ($request->has('partial')) {
                $rem = $medicine->inventories->sum('quantity') - $medicine->min_quantity;
            }
            $resp['dispenced'] = $rem;
            foreach ($medicine->inventories as $inventory) {
                if ($rem <= 0) {
                    break;
                } else {
                    $sub = $rem - $inventory->quantity;
                    $need = $sub <= 0 ?  $rem : $inventory->quantity;
                    $req->dispensings()->syncWithoutDetaching([
                        $inventory->id => ['quantity' => $need]
                    ]);
                    $rem -= $need;
                }
            }
            $resp['message'] = 'Requested medicine dispatched successfully';
        }

        return response()->json($resp, 200);
    }
}
