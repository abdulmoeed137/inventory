<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\PanelResource;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\RoleResource;
use App\Http\Resources\SimplePermissionResource;
use App\Models\Department;
use App\Models\Panel;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('panels', 'departments', 'permissions:id')->where('id', '<>', 1)->orderBy('name')->get();
        $panels = Panel::with('permissions')->select('id', 'title')->orderBy('title')->get();
        $departments = Department::select('id', 'title')->orderBy('title')->get();

        return [
            'roles' => RoleResource::collection($roles),
            'panels' => PanelResource::collection($panels),
            'departments' => DepartmentResource::collection($departments),
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->merge(['isUser' => filter_var($request->isUser, FILTER_VALIDATE_BOOLEAN)]);
            $request->validate(
                [
                    'name' => 'required|string|min:2|max:255|unique:roles,name',
                    'panels' => 'nullable|array|distinct',
                    'panels.*' => 'required|integer|exists:panels,id',
                    'permissions' => 'nullable|array|distinct',
                    'permissions.*' => 'required|integer|exists:permissions,id',
                    'departments' => 'nullable|array|distinct',
                    'departments.*' => 'required|integer|exists:departments,id',
                ],
                [
                    'panels.*.exists' => 'An invalid panel is given.',
                    'permissions.*.exists' => 'An invalid permission is given.',
                    'departments.*.exists' => 'An invalid department is given.'
                ]
            );

            $role = new Role();
            $role->name = $request->name;
            $role->is_user = $request->isUser ? 1 : 0;
            $role->save();
            if ($request->isUser) {
                if (!!$request->panels) {
                    $role->panels()->attach($request->panels);
                }
                if (!!$request->permissions) {
                    $role->permissions()->attach($request->permissions);
                }
            }
            if (!!$request->departments) {
                $role->departments()->attach($request->departments);
            }

            return response()->json([
                'message' => 'Role created successfully.',
                'success' => true,
                'role' => new RoleResource($role->load('panels', 'departments', 'permissions:id'))
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try {
            $request->merge(['isUser' => filter_var($request->isUser, FILTER_VALIDATE_BOOLEAN)]);
            $request->validate(
                [
                    'name' => 'required|string|min:2|max:255|unique:roles,name,' . $role->id,
                    'panels' => 'nullable|array|distinct',
                    'panels.*' => 'required|integer|exists:panels,id',
                    'permissions' => 'nullable|array|distinct',
                    'permissions.*' => 'required|integer|exists:permissions,id',
                    'departments' => 'nullable|array|distinct',
                    'departments.*' => 'required|integer|exists:departments,id',
                ],
                [
                    'panels.*.exists' => 'An invalid panel is given.',
                    'permissions.*.exists' => 'An invalid permission is given.',
                    'departments.*.exists' => 'An invalid department is given.'
                ]
            );

            $role->name = $request->name;
            $role->is_user = $request->isUser ? 1 : 0;
            $role->save();
            if ($request->isUser) {
                if (!!$request->panels) {
                    $role->panels()->sync($request->panels);
                }
                if (!!$request->permissions) {
                    $role->permissions()->sync($request->permissions);
                }
            }
            if (!!$request->departments) {
                $role->departments()->sync($request->departments);
            }
            return response()->json([
                'message' => 'Role updated successfully.',
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
