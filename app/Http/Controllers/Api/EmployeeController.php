<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccountResource;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\RoleResource;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('departments:id')->where('id', '<>', 1)->orderBy('name')->get();
        $departments = Department::orderBy('title')->get();
        $employees = Employee::with('account', 'role', 'department')->orderBy('role_id')->orderBy('created_at', 'desc')->latest()->get();
        return [
            'employees' => (EmployeeResource::collection($employees)),
            'departments' => (DepartmentResource::collection($departments)),
            'roles' => (RoleResource::collection($roles)),
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $needAccount = filter_var($request->needAccount, FILTER_VALIDATE_BOOLEAN);
            $validated = $request->validate([
                'name' => 'required|string|min:3|max:255',
                'number' => 'required|string|max:255|unique:employees,number',
                'email' => 'required|string|email|max:255|unique:employees,email',
                'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:employees,phone',
                'gender' => 'required|integer|between:1,2',
                'password' => 'required_if:needAccount,true|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[\+#\?!\@$%^&\*\-_]).+$/',
                'role' => 'required|integer|exists:roles,id|min:2',
                'department' => 'required|integer|exists:departments,id'
            ]);
            $employee = Employee::create([
                'name' => $validated['name'],
                'number' => $validated['number'],
                'email' => $validated['email'],
                'phone' => $validated['phone'],
                'gender' => $validated['gender'],
                'role_id' => $validated['role'],
                'department_id' => $validated['department'],
            ]);
            if ($needAccount) {
                $employee->account()->create([
                    'email' => $validated['email'],
                    'password' => Hash::make($validated['password']),
                ]);
            }
            return response()->json([
                'message' => 'Employee stored successfully.',
                'success' => true,
                'employee' => (new EmployeeResource($employee->load('account', 'role', 'department')))
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        try {
            $isSelf = $employee->id == auth()->user()->employee_id;
            $data = ['message' => ''];
            $rules = [];
            if (($request->has('password') || $request->has('old_password')) && !$request->has('needAccount')) {
                $rules = [
                    'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[\+#\?!\@$%^&\*\-_]).+$/'
                ];
                if ($isSelf) {
                    $rules['password'] = $rules['password'] . '|different:old_password';
                    $rules['old_password'] = 'required|min:8|password';
                }
                $request->validate($rules);
                $data['message'] = 'Password updated successfully.';
                if ($employee->has('account')) {
                    $employee->account()->update(['password' => Hash::make($request->password)]);
                }
            } else {
                // $request->merge([
                //     'needAccount' => filter_var($request->needAccount, FILTER_VALIDATE_BOOLEAN)
                // ]);
                $rules = [
                    'name' => 'required|string|min:3|max:255',
                    'email' => 'required|email:filter|unique:employees,email,' . $employee->id,
                    'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:employees,phone,' . $employee->id,
                    'picture' => 'nullable|file|mimes:jpeg,png,jpg',
                    'password' => 'required_if:needAccount,true|string|max:8|confirmed',
                ];

                if (!$isSelf) {
                    $rules['gender'] = 'required|integer|between:1,2';
                    $rules['role'] = 'required|exists:roles,id|integer|min:2';
                    $rules['department'] = 'required|integer|exists:departments,id';
                    $rules['number'] = 'required|string|max:255|unique:employees,number,' . $employee->id;
                }
                $request->validate($rules);
                $employee->name = $request->name;
                $employee->phone = $request->phone;
                $account = $employee->account();
                $hasAccount = $account->exists();
                if ($employee->email != $request->email && $hasAccount) {
                    $account->update(['email' => $request->email]);
                }
                $employee->email = $request->email;
                if ($request->hasFile('picture')) {
                    if (isset($employee->picture)) {
                        $path = public_path($employee->picture);
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $employee->picture = '/' . str_replace('public', 'storage', $request->picture->store("public/images/dps"));
                }

                if (!$isSelf) {
                    if ($employee->role_id != $request->role) {
                        $employee->role_id = $request->role;
                        $role = Role::find($request->role);
                        if ($role->is_user == 1 && !$hasAccount) {
                            $temp = $account->create([
                                'email' => $employee->email,
                                'password' => Hash::make($request->password),
                                'status' => 1
                            ]);
                            $temp = new AccountResource($temp);
                            $data['account'] = $temp;
                        } else if ($role->is_user == 0 && $hasAccount) {
                            $account->delete();
                        }
                    }
                    $employee->gender = $request->gender;
                    $employee->department_id = $request->department;
                    $employee->number = $request->number;
                }

                $data['message'] = 'Employee updated successfully.';
            }
            if ($employee->save()) {
                $data['success'] = true;
            }
            return response()->json($data, 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }

    public function toggle(Employee $employee)
    {
        $account = $employee->account;
        $account->status = $account->status == 1 ? 0 : 1;
        if ($account->status == 1) {
            $msg = 'Account has been activated successfully';
        } else {
            $msg = 'Account has been deactivated successfully';
        }
        if ($account->save()) {
            return response()->json(['message' => $msg, 'success' => true], 200);
        }
    }
}
