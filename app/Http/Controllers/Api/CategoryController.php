<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MainCategoryResource;
use App\Http\Resources\SubCategoryResource;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $data = ['categories' => null];
        if ($type == 'main') {
            $categories = Category::latest()->get();
            $data['categories'] = MainCategoryResource::collection($categories);
        } else {
            $categories = SubCategory::with('category')->latest()->get();
            $data['categories'] = SubCategoryResource::collection($categories);
            $mains = Category::latest()->get();
            $data['mains'] = MainCategoryResource::collection($mains);
        }
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        try {
            $isSub = $type == 'sub';
            $category = null;
            $rules = [];
            if ($isSub) {
                $category = new SubCategory();
                $category->category_id = $request->main_category;
                $rules = [
                    'name' => 'required|string|unique:sub_categories,name',
                    'main_category' => 'required|exists:categories,id'
                ];
            } else {
                $category = new Category();
                $rules = [
                    'name' => 'required|string|unique:categories,name',
                ];
            }
            $request->validate($rules);

            $category->name = $request->name;
            $category->status = 1;
            $category->save();
            if (!$isSub) {
                $category = new MainCategoryResource($category);
            } else {
                $category = new SubCategoryResource($category->load('category'));
            }

            return response()->json([
                'message' => 'Category created successfully.',
                'success' => true,
                'category' => $category
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $type)
    {
        try {
            $isSub = $type == 'sub';
            $category = null;
            $rules = [];
            if ($isSub) {
                $category = SubCategory::findOrFail($id);
                $rules = [
                    'name' => 'required|string|unique:sub_categories,name,' . $category->id,
                    'main_category' => 'required|exists:categories,id'
                ];
            } else {
                $category = Category::findOrFail($id);
                $rules = [
                    'name' => 'required|string|unique:categories,name,' . $category->id,
                ];
            }
            $request->validate($rules);
            if ($isSub) {
                $category->category_id = $request->main_category;
            }
            $category->name = $request->name;
            $category->save();

            return response()->json(['message' => 'Category updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    public function toggle($id, $type)
    {
        $category = null;
        if ($type == "sub") {
            $category = SubCategory::findOrFail($id);
        } else {
            $category = Category::findOrFail($id);
        }
        $category->status = $category->status == 1 ? 0 : 1;
        if ($category->status == 1) {
            $msg = 'category has been activated successfully';
        } else {
            $msg = 'category has been deactivated successfully';
        }
        if ($category->save()) {
            return response()->json(['message' => $msg, 'success' => true], 200);
        }
    }
}
