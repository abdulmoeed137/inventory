<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\SimpleResource;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::orderBy('title')->get();
        return ['departments' => DepartmentResource::collection($departments)];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required|string|unique:departments,title'
            ]);

            $department = new Department();
            $department->title = $request->title;
            $department->status = 1;
            $department->save();
            return response()->json([
                'message' => 'Department created successfully.',
                'success' => true,
                'department' => new DepartmentResource($department)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        try {
            $request->validate([
                'title' => 'required|string|unique:departments,title,' . $department->id
            ]);

            $department->title = $request->title;
            $department->status = 1;
            $department->save();
            return response()->json(['message' => 'Department updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        //
    }

    public function toggle(Department $department)
    {
        $department->status = $department->status == 1 ? 0 : 1;
        if ($department->status == 1) {
            $msg = 'Department has been activated successfully';
        } else {
            $msg = 'Department has been deactivated successfully';
        }
        if ($department->save()) {
            return response()->json(['message' => $msg, 'success' => true], 200);
        }
    }

    public function employees(Department $department, $for = '')
    {
        if ($for == 'medicine') {
            $requesters = $department->employees()->has('account')->get();
            $hods = $department->employees()->where('role_id', 2)->get();
            $hons = $department->employees()->where('role_id', 4)->get();

            return [
                'requesters' => SimpleResource::collection($requesters),
                'hods' => SimpleResource::collection($hods),
                'hons' => SimpleResource::collection($hons),
            ];
        } else {
            $employees = $department->employees()->get();
            return ['employees' => EmployeeResource::collection($employees)];
        }
    }
}
