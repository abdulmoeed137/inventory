<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FactoryResource;
use App\Models\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FactoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factories = Factory::latest()->get();
        return response(['factories' => FactoryResource::collection($factories)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string|unique:factories,name',
                'address' => 'nullable|string'
            ]);

            $factory = new Factory();
            $factory->name = $request->name;
            $factory->address = $request->address;
            $factory->status = 1;
            $factory->save();
            return response()->json([
                'message' => 'Factory created successfully.',
                'success' => true,
                'factory' => new FactoryResource($factory)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Factory  $factory
     * @return \Illuminate\Http\Response
     */
    public function show(Factory $factory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Factory  $factory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factory $factory)
    {
        try {
            $request->validate([
                'name' => 'required|string|unique:factories,name,' . $factory->id,
                'address' => 'nullable|string'
            ]);

            $factory->name = $request->name;
            $factory->address = $request->address;
            $factory->save();
            return response()->json(['message' => 'Factory updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factory  $factory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factory $factory)
    {
        //
    }

    public function toggle(Factory $factory)
    {
        $factory->status = $factory->status == 1 ? 0 : 1;
        if ($factory->status == 1) {
            $msg = 'Factory has been activated successfully';
        } else {
            $msg = 'Factory has been deactivated successfully';
        }
        if ($factory->save()) {
            return response()->json(['message' => $msg, 'success' => true], 200);
        }
    }
}
