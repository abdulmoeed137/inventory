<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MedicineInventoryRequest;
use App\Http\Resources\MedicineDepartmentResource;
use App\Http\Resources\MedicineExpirationResource;
use App\Http\Resources\MedicineInventoryResource;
use App\Http\Resources\MedicineMovementResource;
use App\Http\Resources\MedicineOrderResource;
use App\Http\Resources\MedicineResource;
use App\Http\Resources\SupplierResource;
use App\Models\Medicine;
use App\Models\MedicineInventory;
use App\Models\MedicineOrder;
use App\Models\MedicineRequest;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ['medicines' => MedicineResource::collection(Medicine::latest()->get())];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'description' => 'required|string',
                'uses' => 'required|string|max:255',
                'side_effects' => 'required|string|max:255',
                'min_quantity' => 'required|integer|min:1'
            ]);
            $medicine = Medicine::create($validated + ['status' => true]);
            return response()->json([
                'message' => 'Medicine created successfully.',
                'success' => true,
                'medicine' => new MedicineResource($medicine)
            ], 201);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function show(Medicine $medicine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medicine $medicine)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'description' => 'required|string',
                'uses' => 'required|string|max:255',
                'side_effects' => 'required|string|max:255',
                'min_quantity' => 'required|integer|min:1'
            ]);
            $medicine->update($validated + ['status' => true]);
            return response()->json([
                'message' => 'Medicine updated successfully.',
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medicine $medicine)
    {
        //
    }

    public function toggle(Medicine $medicine)
    {
        $medicine->update(['status' => $medicine->status == 1 ? 0 : 1]);
        $ac = $medicine->status == 1 ? 'activated' : 'deactivated';
        return response()->json(['message' => "Item has been $ac successfully", 'success' => true], 200);
    }

    public function inventories()
    {
        return [
            'medicines' => MedicineResource::collection(Medicine::where('status', 1)->latest()->get()),
            'inventories' => MedicineInventoryResource::collection(MedicineInventory::withCount('dispensings as dispensed_count')->with('medicine')->latest()->get())
        ];
    }

    public function createInventory(MedicineInventoryRequest $request)
    {
        try {
            $validated = $request->validated();
            $inventory = new MedicineInventory();
            $inventory = $inventory->storeOrUpdate($validated);
            return response()->json([
                'message' => 'Medicine inventory created successfully.',
                'success' => true,
                'inventory' => new MedicineInventoryResource($inventory->load('medicine'))
            ], 201);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function updateInventory(MedicineInventoryRequest $request, $id)
    {
        try {
            $inventory = MedicineInventory::findOrFail($id);
            $validated = $request->validated();
            $inventory->storeOrUpdate($validated);
            return response()->json([
                'message' => 'Medicine inventory updated successfully.',
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function orders()
    {
        return [
            'orders' => MedicineOrderResource::collection(MedicineOrder::withAll()),
            'medicines' => MedicineResource::collection(Medicine::where('status', 1)->latest()->get()),
            'suppliers' => SupplierResource::collection(Supplier::where('type', 3)->latest()->get()),
        ];
    }

    public function createOrder(Request $request)
    {
        try {
            $validated = $request->validate([
                'quantity' => 'required|integer|min:0',
                'total_price' => 'required|numeric|min:0',
                'supplied_at' => 'required|date',
                'medicine' => 'required|integer|exists:medicines,id,status,1',
                'supplier' => 'required|integer|exists:suppliers,id'
            ]);
            foreach (['medicine', 'supplier'] as $remove) {
                $validated["{$remove}_id"] = $validated[$remove];
                unset($validated[$remove]);
            }
            $order = MedicineOrder::create($validated);
            return response()->json([
                'message' => 'Medicine order created successfully.',
                'success' => true,
                'order' => new MedicineOrderResource($order->loadAll())
            ], 201);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function updateOrder(Request $request, $id)
    {
        try {
            $order = MedicineOrder::findOrFail($id);
            $validated = $request->validate([
                'quantity' => 'required|integer|min:0',
                'total_price' => 'required|numeric|min:0',
                'supplied_at' => 'nullable|date',
                'medicine' => 'required|integer|exists:medicines,id,status,1',
                'supplier' => 'required|integer|exists:suppliers,id'
            ]);
            foreach (['medicine', 'supplier'] as $remove) {
                $validated["{$remove}_id"] = $validated[$remove];
                unset($validated[$remove]);
            }
            if (
                $order->inventory()->exists() &&
                ($order->medicine_id != $validated['medicine_id'] ||
                    $order->quantity != $validated['quantity']
                )
            ) {
                $order->inventory()->update([
                    'medicine_id' => $validated['medicine_id'],
                    'quantity' => $validated['quantity']
                ]);
            }
            $order->update($validated);
            return response()->json([
                'message' => 'Medicine order updated successfully.',
                'success' => true,
                'supplied_at' => $order->supplied_at
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function orderStatus(MedicineInventoryRequest $request, $id)
    {
        try {
            $order = MedicineOrder::findOrFail($id);
            if ($request->status == "complete") {
                $validated = $request->validated();
                $validated['medicine_order_id'] = $id;
                $inventory = new MedicineInventory();
                $inventory->storeOrUpdate($validated);
                $msg = 'Order completed and inventory created successfully.';
            } else {
                $order->inventory()->delete();
                $msg = 'Order reverted successfully.';
            }

            return response()->json([
                'message' => $msg,
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function inventoryMovementReport($period = '')
    {
        // return $reports=::;
        $reports = MedicineInventory::with('medicine')
            ->whereMonth('expired_at', '<=', now()->addMonths(6)->format('m'))->orderBy('expired_at')->get();

        return $reports = MedicineRequest::with('medicine', 'department')->get();
    }

    public function movement(Request $request)
    {
        $period = $request->period;
        $reports = DB::table('medicine_dispensings', 'md')
            ->select(['m.name as medicine', 'md.quantity as quantity', 'd.title as department', 'md.created_at as dispensed_at'])
            ->join('medicine_requests as r', 'r.id', '=', 'md.request_id')
            ->join('medicines as m', 'm.id', '=', 'r.medicine_id')
            ->join('departments as d', 'd.id', '=', 'r.department_id')
            ->when($period == 1, fn ($q) => $q->whereMonth('md.created_at', now()->format('m'))->whereYear('md.created_at', now()->format('Y')))
            ->when($period == 2, fn ($q) => $q->whereBetween('md.created_at', [now()->startOfWeek(), now()->endOfWeek()]))
            ->when($period == 3, fn ($q) => $q->whereMonth('md.created_at', '!=', now()->format('m'))->whereYear('md.created_at', now()->format('Y')))
            ->orderBy('dispensed_at')->get();

        return  ['reports' => MedicineMovementResource::collection($reports)];
    }

    public function expiration()
    {
        $reports = MedicineInventory::with('medicine')
            ->whereMonth('expired_at', '<=', now()->addMonths(6)->format('m'))->orderBy('expired_at')->get();
        return  ['reports' => MedicineExpirationResource::collection($reports)];
    }

    public function department()
    {
        $reports = Medicine::select('name as medicine', 'title as department', DB::raw('IFNULL(SUM(quantity), 0) AS quantity'))
            ->join('medicine_requests as mr', 'mr.medicine_id', 'medicines.id')
            ->join('departments as d', 'd.id', 'mr.department_id')
            ->groupBy('name', 'title', 'medicine_id')
            ->get();
        return  ['reports' => MedicineDepartmentResource::collection($reports)];
    }
}
