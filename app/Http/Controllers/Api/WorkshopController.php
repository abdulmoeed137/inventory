<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WorkshopResource;
use App\Models\Workshop;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response([
            'workshops' => WorkshopResource::collection(Workshop::latest()->get())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'manager' => 'required|string|max:255',
                'engineer' => 'required|string|max:255',
                'date' => 'required|date',
            ]);
            $workshop = Workshop::create($validated);
            return response()->json([
                'message' => 'Workshop created successfully.',
                'success' => true,
                'workshop' => new WorkshopResource($workshop)
            ], 200);
        } catch (\Exception $e) {
            $array = [
                'message'    => 'Error',
                'status' => 'error',
                'errors' => $e->getMessage(),
                'trace' => $e->getTrace()
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $array['errors'] = $e->errors();
                $code = 422;
            }
            return response()->json($array, $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function show(Workshop $workshop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workshop $workshop)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'manager' => 'required|string|max:255',
                'engineer' => 'required|string|max:255',
                'date' => 'required|date',
            ]);
            $workshop->update($validated);
            return response()->json([
                'message' => 'Workshop updated successfully.',
                'success' => true,
                'show_date' => $workshop->show_date
            ], 200);
        } catch (\Exception $e) {
            $array = [
                'message'    => 'Error',
                'status' => 'error',
                'errors' => $e->getMessage(),
                'trace' => $e->getTrace()
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $array['errors'] = $e->errors();
                $code = 422;
            }
            return response()->json($array, $code);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workshop $workshop)
    {
        $workshop->delete();
        return response()->json([
            'message' => 'Workshop deleted successfully.',
            'success' => true
        ], 200);
    }
}
