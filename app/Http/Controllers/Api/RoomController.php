<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\RoomResource;
use App\Models\Department;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::orderBy('title')->get();
        $rooms = Room::with('department')->orderBy('title')->latest()->get();
        return [
            'rooms' => RoomResource::collection($rooms),
            'departments' => DepartmentResource::collection($departments)
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required|string|unique:rooms,title,null,id,department_id,' . $request->department,
                'department' => 'required|integer|exists:departments,id'
            ]);

            $room = new Room();
            $room->title = $request->title;
            $room->department_id = $request->department;
            $room->status = 1;
            $room->save();
            return response()->json([
                'message' => 'Room created successfully.',
                'success' => true,
                'room' => new RoomResource($room->load('department'))
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        try {
            $request->validate([
                'title' => 'required|string|unique:rooms,title,' . $room->id . ',id,department_id,' . $request->department,
                'department' => 'required|integer|exists:departments,id'
            ]);

            $room->title = $request->title;
            $room->department_id = $request->department;
            $room->status = 1;
            $room->save();
            return response()->json(['message' => 'Room updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }

    public function toggle(Room $room)
    {
        $room->status = $room->status == 1 ? 0 : 1;
        if ($room->status == 1) {
            $msg = 'Room has been activated successfully';
        } else {
            $msg = 'Room has been deactivated successfully';
        }
        if ($room->save()) {
            return response()->json(['message' => $msg, 'success' => true], 200);
        }
    }
}
