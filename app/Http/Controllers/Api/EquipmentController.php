<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EquipmentResource;
use App\Http\Resources\EquipmentServiceResource;
use App\Http\Resources\MainCategoryResource;
use App\Http\Resources\SupplierResource;
use App\Models\Category;
use App\Models\Department;
use App\Models\Equipment;
use App\Models\EquipmentService;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipments = EquipmentResource::collection(Equipment::getLatest());
        $departments = DepartmentResource::collection(Department::latest()->get());
        $mains = MainCategoryResource::collection(Category::withAndHas('subCategories')->latest()->get());
        $suppliers = SupplierResource::collection(Supplier::has('engineers')->byType('equipment'));
        return response(compact('equipments', 'departments', 'mains', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'bail|required|string|min:2|max:255',
                'serial_no' => "required|string|min:2|max:255|unique:equipment,serial_no,null,id,name,$request->name",
                'model' => 'required|string|min:2|max:255',
                'installation_date' => 'required|date',
                'warrenty' => 'nullable|date',
                'sub_category' => 'required|integer|exists:sub_categories,id',
                'department' => 'required|integer|exists:departments,id',
                'engineer' => 'required|integer|exists:supplier_engineers,id',
                'last_date' => 'nullable|date|before:next_date',
                'next_date' => 'nullable|date|after:last_date',
                'cost' => 'nullable|numeric|min:0|required_with:next_date|required_with:last_date',
            ]);

            $removes = ['engineer', 'department', 'sub_category'];
            $add = ['status' => true];
            $service = array_slice($validated, 8, count($validated));

            foreach ($removes as $key => $remove) {
                $add[$key != 0 ? "{$remove}_id" : 'supplier_engineer_id'] = $validated[$remove];
                unset($validated[$remove]);
            }
            $equipment = Equipment::create($validated + $add);
            if (!array_search('', $service)) {
                $equipment->services()->create($service);
            }
            return response()->json([
                'message' => 'Equipment created successfully.',
                'success' => true,
                'equipment' => new EquipmentResource($equipment->loadRelations())
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function show(Equipment $equipment)
    {
        return response([
            'equipment' =>
            new EquipmentResource($equipment->loadRelations(true))
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipment $equipment)
    {
        try {
            $validated = $request->validate([
                'name' => 'bail|required|string|min:2|max:255',
                'serial_no' => "required|string|min:2|max:255|unique:equipment,serial_no,$equipment->id,id,name,$request->name",
                'model' => 'required|string|min:2|max:255',
                'installation_date' => 'required|date',
                'warrenty' => 'nullable|date',
                'sub_category' => 'required|integer|exists:sub_categories,id',
                'department' => 'required|integer|exists:departments,id',
                'engineer' => 'required|integer|exists:supplier_engineers,id',
            ]);

            $removes = [
                'engineer', 'department', 'sub_category'
            ];
            $add = [];
            foreach ($removes as $key => $remove) {
                $add[$key != 0 ? "{$remove}_id" : 'supplier_engineer_id'] = $validated[$remove];
                unset($validated[$remove]);
            }
            $equipment->update($validated + $add);
            return response()->json([
                'message' => 'Equipment updated successfully.',
                'dates' => [
                    'installation_date' => $equipment->installation_date,
                    'warrenty' => $equipment->warrenty,
                ],
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipment $equipment)
    {
        //
    }

    public function services(Equipment $equipment)
    {
        return response([
            'services' => EquipmentServiceResource::collection($equipment->services()->latest()->get())
        ]);
    }

    public function storeService(Request $request)
    {
        try {
            $validated = $request->validate([
                'last_date' => 'nullable|date|before:next_date|required_if:next_date,nullable',
                'next_date' => 'nullable|date|after:last_date|required_if:last_date,nullable',
                'cost' => 'required|numeric|min:0',
                'equipment' => 'required|integer|exists:equipment,id'
            ]);

            $validated['equipment_id'] = $validated['equipment'];
            unset($validated['equipment']);

            $service = EquipmentService::create($validated);
            return response()->json([
                'message' => 'Equipment created successfully.',
                'success' => true,
                'service' => new EquipmentServiceResource($service)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    public function updateService(Request $request,  $id)
    {
        try {
            $service = EquipmentService::findOrFail($id);
            $validated = $request->validate([
                'last_date' => 'nullable|date|before:next_date|required_without:next_date',
                'next_date' => 'nullable|date|after:last_date|required_without:last_date',
                'cost' => 'required|numeric|min:0',
            ]);

            $service->update($validated);
            return response()->json([
                'message' => 'Equipment updated successfully.',
                'success' => true,
                'data' => ['dates' => [$service->last_date, $service->next_date]]
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }
}
