<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierResource;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'inventory')
    {
        return response([
            'suppliers' => SupplierResource::collection(Supplier::byType($type)),
            'type' => $type
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $types = ['inventory', 'equipment', 'medicine'];
            $validated = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:suppliers,email',
                'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:suppliers,phone',
                'address' => 'required|string',
                'locale' => 'required|integer|between:0,2',
                'type' => 'required|in:equipment,inventory,medicine',
            ]);
            $validated['type'] = array_search($validated['type'], $types) + 1;
            $supplier = Supplier::create($validated + ['status' => true]);
            return response()->json([
                'message' => 'Supplier created successfully.',
                'success' => true,
                'supplier' => new SupplierResource($supplier)
            ], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        try {
            $validated =  $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:suppliers,email,' . $supplier->id,
                'phone' => 'required|string|min:7|max:15|regex:/^[\+]?\d+/|unique:suppliers,phone,' . $supplier->id,
                'address' => 'required|string',
                'locale' => 'required|integer|between:0,2'
            ]);

            $supplier->update($validated);
            return response()->json(['message' => 'Supplier updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'status' => 'error',
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }

    public function toggle(Supplier $supplier)
    {
        $supplier->update(['status' => !(!!$supplier->status)]);
        return response()->json(['message' => 'supplier has been ' . (!!$supplier->status ? 'activated' : 'deactivated') . ' successfully', 'success' => true], 200);
    }
}
