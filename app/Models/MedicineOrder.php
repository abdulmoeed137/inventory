<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicineOrder extends Model
{
    // const status = ['initiated', 'completed'];

    use HasFactory;

    protected $fillable = ['quantity', 'supplier_id', 'medicine_id', 'supplied_at', 'total_price'];

    public function scopeWithAll($query)
    {
        return $query->withCount(['inventory', 'inventory as dispensed_count' => fn ($q) => $q->has('dispensings')])->with('medicine', 'supplier')->latest()->get();
    }

    public function loadAll()
    {
        return $this->loadCount(['inventory', 'inventory as dispensed_count' => fn ($q) => $q->has('dispensings')])->load('medicine', 'supplier');
    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class)->select('id', 'name');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select('id', 'name');
    }

    public function inventory()
    {
        return $this->hasOne(MedicineInventory::class);
    }

    public function getSuppliedAtAttribute($value)
    {
        return !$value ? $value : Carbon::parse($value)->format('d-M-Y h:i a');
    }
}
