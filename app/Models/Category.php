<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'status'];
    protected $casts = [
        'status' => 'boolean'
    ];

    public function scopeWithAndHas($query, $relation)
    {
        return $query->has($relation)->with($relation);
    }

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class)->orderBy('name');
    }
}
