<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    const Types = ['inventory' => 1, 'equipment' => 2, 'medicine' => 3];
    const Locale = ['Local', 'International', 'City'];
    protected $fillable = ['name', 'email', 'phone', 'address', 'locale', 'type', 'status'];

    public function scopeByType($query, $type)
    {
        return $query->where('type', self::Types[$type])->latest()->get();
    }

    public function engineers()
    {
        return $this->hasMany(SupplierEngineer::class);
    }

    public function getLocaleAttribute($value)
    {
        return !is_null($value) ? self::Locale[$value] : null;
    }
}
