<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'department_id', 'status'];
    protected $casts = [
        'status' => 'boolean'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
