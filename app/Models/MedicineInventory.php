<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicineInventory extends Model
{
    use HasFactory;
    protected $fillable = ['patch_no', 'quantity', 'expired_at', 'produced_at', 'medicine_id', 'medicine_order_id'];

    public function storeOrUpdate($data)
    {
        $data['medicine_id'] = $data['medicine'];
        unset($data['medicine']);
        if (!!$this->id) {
            if (
                $this->order()->exists() &&
                ($this->medicine_id != $data['medicine_id'] ||
                    $this->quantity != $data['quantity'])
            ) {
                $this->order()->update([
                    'medicine_id' => $data['medicine_id'],
                    'quantity' => $data['quantity']
                ]);
            }
            return $this->update($data);
        }
        return $this->create($data);
    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class)->select('id', 'name');
    }

    public function order()
    {
        return $this->belongsTo(MedicineOrder::class, 'medicine_order_id');
    }

    public function dispensings()
    {
        return $this->belongsToMany(MedicineRequest::class, 'medicine_dispensings', 'inventory_id', 'request_id')
            ->withPivot('quantity');
    }

    public function getExpiredAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getProducedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
