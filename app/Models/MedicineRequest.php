<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MedicineRequest extends Model
{
    use HasFactory;

    protected $fillable = ['department_id', 'approver_id', 'requester_id', 'hod_id', 'hon_id', 'pharmacist_id', 'medicine_id', 'quantity', 'type'];

    private $allRelations = ['medicine', 'department', 'approver', 'requester', 'hod', 'hon', 'pharmacist'];

    public function loadAllRelations()
    {
        return $this->loadSum(['dispensings as given_quantity' => fn ($q) => $q->select(DB::raw('CAST(IFNULL(SUM(medicine_dispensings.quantity), 0) AS UNSIGNED)'))], 'medicine_dispensings.quantity')->load($this->allRelations);
    }

    public function scopeWithAllRelations($query)
    {
        return $query->withSum(['dispensings as given_quantity' => fn ($q) => $q->select(DB::raw('CAST(IFNULL(SUM(medicine_dispensings.quantity), 0) AS UNSIGNED)'))], 'medicine_dispensings.quantity')->with($this->allRelations)->latest()->get();
    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class)->select('id', 'name', 'min_quantity');
    }

    public function department()
    {
        return $this->belongsTo(Department::class)->select('id', 'title');
    }

    public function approver()
    {
        return $this->belongsTo(Employee::class, 'approver_id')->select('id', 'name');
    }

    public function requester()
    {
        return $this->belongsTo(Employee::class, 'requester_id')->select('id', 'name');
    }
    public function hod()
    {
        return $this->belongsTo(Employee::class, 'hod_id')->select('id', 'name');
    }
    public function hon()
    {
        return $this->belongsTo(Employee::class, 'hon_id')->select('id', 'name');
    }
    public function pharmacist()
    {
        return $this->belongsTo(Employee::class, 'pharmacist_id')->select('id', 'name');
    }

    public function dispensings()
    {
        return $this->belongsToMany(MedicineInventory::class, 'medicine_dispensings', 'request_id', 'inventory_id')
            ->withPivot('quantity')->withTimestamps();
    }
}
