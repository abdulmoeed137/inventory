<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'serial_no',
        'model',
        'department_id',
        'sub_category_id',
        'supplier_engineer_id',
        'installation_date',
        'warrenty',
        'status'
    ];

    protected $casts = [
        'installation_date' => 'datetime:d-M-Y h:i a',
        'created_at' => 'datetime:d-m-Y h:i a',
    ];

    private function relations()
    {
        return [
            'department:id,title',
            'subCategory' => fn ($q) => $q->select('id', 'name', 'category_id')->with('category:id,name'),
            'supplierEngineer.supplier',
            'latestService'
        ];
    }

    public function loadRelations($all = false)
    {
        $arr = $this->relations();
        if (!$all) {
            $arr[1] .= ':id,name';
        }
        return $this->load($arr)->loadSum('services as serviceCost', 'cost');
    }

    public function scopeGetLatest($query)
    {
        return $query->with($this->relations())->withSum('services as serviceCost', 'cost')->latest()->get();
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function supplierEngineer()
    {
        return $this->belongsTo(SupplierEngineer::class);
    }

    public function services()
    {
        return $this->hasMany(EquipmentService::class);
    }

    public function latestService()
    {
        return $this->hasOne(EquipmentService::class)->orderBy('id', 'desc');
    }

    public function getInstallationDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-M-Y h:i a');
    }

    public function getWarrentyAttribute($value)
    {
        return !$value ? "" : Carbon::parse($value)->format('d-M-Y h:i a');
    }

    public function getServiceCostAttribute($value)
    {
        return number_format($value);
    }
}
