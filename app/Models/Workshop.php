<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'manager', 'engineer', 'date'];
    protected $appends = ['show_date'];

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function getShowDateAttribute()
    {
        $date = Carbon::parse($this->date);
        return [
            $date->format('d-m-Y h:i a'),
            $date->format('l')
        ];
    }
}
