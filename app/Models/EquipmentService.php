<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipmentService extends Model
{
    use HasFactory;
    protected $fillable = [
        'last_date',
        'next_date',
        'cost',
        'equipment_id',
    ];

    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    public function getLastDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-M-Y h:i a');
    }

    public function getNextDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-M-Y h:i a');
    }
}
