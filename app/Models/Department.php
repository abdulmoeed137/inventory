<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'status'];
    protected $casts = [
        'status' => 'boolean'
    ];

    public function scopeWithAndHas($query, $relation)
    {
        return $query->has($relation)->with($relation);
    }
    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)->with([$relation => $constraint]);
    }

    public function rooms()
    {
        $this->hasMany(Room::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class)->orderBy('name');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    // public function accounts()
    // {
    //     return $this->hasManyThrough(Account::class, Employee::class);
    // }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = strtolower($value);
    }

    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }
}
