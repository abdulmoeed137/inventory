<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description', 'uses', 'side_effects', 'min_quantity', 'status'];

    public function inventories()
    {
        return $this->hasMany(MedicineInventory::class);
    }

    public function requests()
    {
        return $this->hasMany(MedicineRequest::class);
    }
}
